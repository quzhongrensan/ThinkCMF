<?php

/**
 * 系统配置文件
 */
return array(
    'DB_TYPE' => 'mysql',
    'DB_HOST' => 'localhost',
    'DB_NAME' => 'cmf_think',
    'DB_USER' => 'root',
    'DB_PWD' => 'anyons',
    'DB_PORT' => '3306',
    'DB_PREFIX' => 'cmf_',
    /* Default Module */
    'DEFAULT_MODULE' => 'Portal',
    /* Data Auth Key */
    "DATA_AUTH_KEY" => 'K6NH9EWp5S4ZEFNnvW',
    /* cookies Prefix */
    "COOKIE_PREFIX" => 'DpTFTX_',
    /* Potal Tpl Path */
    'CMF_TPL_PATH' => APP_ROOT . '/static/Portal/',
    /* CMF Config Path */
    'CMF_CONF_PATH' => CMF_DATA . '/config/',
    /* CMF Databack Path */
    'CMF_DATA_PATH' => CMF_DATA . '/backup/',
    /* TMPL Parse String  */
    'TMPL_PARSE_STRING' => array(
        '__TMPL__' => __ROOT__ . '/static/Portal/default',
    ),
    'AUTOLOAD_NAMESPACE' => array(
        'Library' => COMMON_PATH,
    )
);

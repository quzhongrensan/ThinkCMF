<?php

/* 开启调试模式 */
define("APP_DEBUG", TRUE);

/* SESSION 会控制 */
@session_name(md5(__FILE__));

/* 定义应该根目录 */
define('APP_ROOT', str_replace('\\', '/', getcwd()) . '/');

/* 项目路径，不可更改 */
define('APP_PATH', APP_ROOT . '/Module/');

/* 数据写入目录 */
define('CMF_DATA', APP_ROOT . '/static/data/');

/* 定义基础核心组件路径 */
define('COMMON_PATH', APP_ROOT . 'Library/');

/* 定义配置文件路径 */
define("CONF_PATH", CMF_DATA . '/config/');

/* 定义缓存存放路径 */
define("RUNTIME_PATH", CMF_DATA . '/runtime/');

/* 检测系统是否需要安装 */
file_exists(CMF_DATA . "install.lock") or $_GET['m'] = 'Install';

/* 版本号 */
define("APP_VERSION", '1.2');

//载入框架核心文件
require COMMON_PATH . 'Think/ThinkPHP.php';
